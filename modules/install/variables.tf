variable "namespace" {
  default = ""
}
variable "chart_version" {
  default = "2.5.1"
}

variable "extra_values" {
  default = {}
}
