
locals {
  EXTRA_VALUES = {
  }
}


resource "helm_release" "release" {
  name       = "home-assistant"
  repository = "https://k8s-at-home.com/charts"
  chart      = "home-assistant"
  version    = var.chart_version
  namespace  = var.namespace
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]
}
